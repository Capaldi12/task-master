package com.nikadmin.taskmaster;

/* subtask only exists as part of task
 * can be checked
 * knows it's parent
 */
public class Subtask extends Item {
    private boolean _checked;
    private long _supertask;

    public Subtask(long id, String name, long supertask) { super(id, name); _supertask = supertask; }

    public boolean isChecked() { return _checked; }
    public void setChecked(boolean b) { _checked = b; }

    public void check() { setChecked(true); }
    public void uncheck() { setChecked(false); }

    public long supertask() { return _supertask; }
    public void setSupertask(long s) { _supertask = s; }
}
